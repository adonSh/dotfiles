" BASIC BEHAVIOUR
syntax on
set ruler
set nowrap
set nobackup
set nohlsearch
set splitright
set splitbelow
set laststatus=1
set background=light

" FORMATTING
set textwidth=79
set colorcolumn=81
set formatoptions=j

" INDENTATION
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set noautoindent
set nocindent
"set nosmartindent

" COLORS
hi Visual ctermbg=244
